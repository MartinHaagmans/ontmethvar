rule bam_call_snv:
    input:
       bam = rules.dorado_align.output.bam_aligned
    output:
        finished = "log/{sample}.clair3.finished"
    conda:
        'envs/clair3.yaml'
    resources:
        mem_mb=config['CLAIR3_MEM'],
        threads=config['CLAIR3_THREADS']
    params:
        prefix = "{sample}_clair3"
    log:
        "log/{sample}.clair3.log"
    shell:
        '''
        run_clair3.sh \\
        --bam_fn={input.bam} \\
        --output={params.prefix} \\
        --ref_fn={HG38FA} \\
        --model_path={CLAIR3_MODEL} \\
        --threads={resources.threads} \\
        --platform=ont > {log} 2>&1
        touch {output.finished}
         '''


rule bam_call_sv:
    input:
       bam = rules.dorado_align.output.bam_aligned
    output:
        vcf = "{sample}_sniffles.vcf",
        finished = "log/{sample}.sniffles.finished",
    conda:
        'envs/sniffles2.yaml'
    resources:
        mem_mb=config['SNIFFLES_MEM'],
        threads=config['SNIFFLES_THREADS']
    params:
        prefix = "{sample}_clair3"
    log:
        "log/{sample}.sniffles.log"
    shell:
        '''
        sniffles \\
        --reference {HG38FA} \\
        --input {input.bam} \\
        --vcf {output.vcf} > {log} 2>&1
        touch {output.finished}
        '''


rule bam_call_methylation:
    input:
       bam = rules.dorado_align.output.bam_aligned
    output:
        bed = "{sample}_modkit.bed",
        finished = "log/{sample}.modkit.finished",
    resources:
        mem_mb=config['MODKIT_MEM'],
        threads=config['MODKIT_THREADS']
    log:
        "log/{sample}.modkit.log"
    shell:
        '''
        {MODKIT} pileup \\
        --threads {resources.threads} \\
        {input.bam} {output.bed} > {log} 2>&1
        touch {output.finished}
         '''


rule bam_call_coverage:
    input:
       bam = rules.dorado_align.output.bam_aligned
    output:
        finished = "log/{sample}.mosdepth.finished"
    resources:
        mem_mb=config['MOSDEPTH_MEM'],
        threads=config['MOSDEPTH_THREADS']
    params:
        prefix = "{sample}_"
    log:
        "log/{sample}.mosdepth.log"
    shell:
        '''
        {MOSDEPTH} \\
        --threads {resources.threads} \\
        --by 500 \\
        {params.prefix} \\
        {input.bam} > {log} 2>&1
        touch {output.finished}
        '''
