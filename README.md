# ONT call variants and methylation

1) Align modified basecalls with dorado/minimap2

2a) Call variants with Clair3

2b) Call structural variants with sniffles2

2c) Call methylation with modkit traditional/modern